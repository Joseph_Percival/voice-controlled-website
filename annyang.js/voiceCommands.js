if (annyang) {

// Let's define a command.
var commands = {
    'back': function() {window.history.back()},
    'forward': function() {window.history.go(1)},
    'home': function() { window.location.assign("/index.html") },
    'basics': function() { window.location.assign("/basics.html") },
    'frets': function() { window.location.assign("/frets.html") },
    'chords': function() { window.location.assign("/chords.html") },
    'rhythm': function() { window.location.assign("/rhythm.html") },
    'lead': function() { window.location.assign("/lead.html") },

    'menu': function() { $("#mobilenavigationwindow").toggleClass('hidden-class')},

}

// Add our commands to annyang
annyang.addCommands(commands);

// Start listening.`
annyang.start();
}
