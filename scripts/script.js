// #########Hover effect over the page navigation ###//

//        calls a function to the document//
        $(function() {
// gets every img element from the div and applies the hover function
  $('#touch-nav-content a img').hover(function() {
      // every item that is hovered over(this) it fades the opacity to 60%
          $(this).fadeTo(50,.6);
//Once the cursor s leaving the img object another function is called
              }, function() {
//This funcction returns the image object to its original opacity once the cursor leaves
            $(this).fadeTo(400,1);
        });
    });
